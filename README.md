This project is my website for the 5th point in computer science class.  
  
Website Layout:  
Website  
|....src (Java Resources)  
|....|....fokc  
|....|....|....website  
|....|....|....|....Hashing.java  
|....|....|....|....SqlUtilities.java  
|....WebContent  
|....|....fonts  
|....|....|....roboto  
|....|....|....|....LISCENSE.txt  
|....|....|....|....Roboto-Italic.ttf  
|....|....|....|....Roboto-Regular.ttf  
|....|....|....|....Roboto-Black.ttf  
|....|....|....|....Roboto-Light.ttf  
|....|....|....|....Roboto-Thin.ttf  
|....|....|....|....Roboto-BlackItalic.ttf  
|....|....|....|....Roboto-LightItalic.ttf  
|....|....|....|....Roboto-ThinItalic.ttf  
|....|....|....|....Roboto-Bold.ttf  
|....|....|....|....Roboto-Medium.ttf  
|....|....|....|....Roboto-BoldItalic.ttf  
|....|....|....|....Roboto-MediumItalic.ttf  
|....|....|....fonts.css  
|....|....functions  
|....|....|....comment.jsp  
|....|....|....hide_comment.jsp  
|....|....|....hide_post.jsp  
|....|....|....login.jsp  
|....|....|....logout.jsp  
|....|....|....manage.jsp  
|....|....|....post.jsp  
|....|....|....register.jsp  
|....|....images  
|....|....|....profiles  
|....|....|....|....fokc.png  
|....|....|....carbon.png  
|....|....|....nanuli.png  
|....|....header.jsp  
|....|....index.jsp  
|....|....manage.jsp  
|....|....register.jsp  
|....|....updates.jsp  
|....|....youtube.jsp  
|....|....styles.css  
