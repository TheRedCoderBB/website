package fokc.website;

import java.security.MessageDigest;

public class Hashing {

	public static String HashSHA256(String str) {
		try {
			MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
			messageDigest.update(str.getBytes());
			return new String(messageDigest.digest());
		} catch (Exception e) {
			return null;
		}
	}
}
