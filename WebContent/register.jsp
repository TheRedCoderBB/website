<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>FokSite - Register</title>

<!-- Style References -->
<link rel="stylesheet" href="styles.css">

<style>

</style>

<script>
    function elementValue(elementId) {
        var element = document.getElementById(elementId)
        if(element == null) return null;
        return element.value;
    }
    function validate() {
        var uid = elementValue("uid");
        var pass = elementValue("pass");
        var email = elementValue("email");
        var alertData = "";
        if(uid == null || !/^[a-zA-Z0-9]+$/.test(uid))
            alertData += "Username contains invalid letters!\n(Only letters and digits allowed)\n\n";
        
        if(pass == null || !(pass.length >= 8 &&
             pass.match(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/) &&
             pass.match(/[0-9]/) &&
             pass.match(/[a-zA-Z]/)))
            alertData += "Password doesn't meet requirements!\n(At least 8 characters long and one digit, letter and symbol)\n\n";
        
        let emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(email == null || !email.match(emailRegex))
            alertData += "Invalid email address!\n\n"
        
        if(alertData.length == 0)
            alert(alertData);
        
        return alertData.length == 0;
    }
</script>
    
</head>
<body>
  <jsp:include page="header.jsp"></jsp:include>
  <section id="main">
    <section>
    </section>
    <form autocomplete="off" method="post" action="functions/register.jsp" onsubmit="return validate()" id="registration" class="main-block">
      <p>Registration</p>
      <input type="text"     class="login-text" placeholder="Username"     name="uid"   required    id="uid"
             style="margin: 5px 0;"><span style="color: red;">&nbsp;*</span>
      <input type="password" class="login-text" placeholder="Password"     name="pass"  required    id="pass"
             style="margin: 5px 0;"><span style="color: red;">&nbsp;*</span>
      <input type="email"    class="login-text" placeholder="Email Adress" name="email" required    id="email"
             style="margin: 5px 0;"><span style="color: red;">&nbsp;*</span>
      <input type="text"     class="login-text" placeholder="First Name"   name="first_name"
             style="margin: 5px 0;"><span style="visibility: hidden;">&nbsp;*</span>
      <input type="text"     class="login-text" placeholder="Last Name"    name="last_name"
             style="margin: 5px 0;"><span style="visibility: hidden;">&nbsp;*</span><br>
      <span style="color: red; font: 16px Roboto Light;">* Required</span><span style="visibility: hidden;">&nbsp;*</span><br>
      <section style="text-align: center; margin: 18px 0 0 0;">
        <button type="submit" class="login-button">Register</button>
        <button type="reset" class="login-button">Reset</button>
      </section>
    </form>
  </section>
</body>
</html>