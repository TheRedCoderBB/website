<%@page import="fokc.website.Hashing"%>
<%@page import="fokc.website.SqlUtilities"%>
<%@page import="java.util.ArrayList"%>
<%@ page import="java.sql.*"%>
<%
    String uid = session.getAttribute("uid").toString();
	String cur_uid = request.getParameter("cur_uid");
	String new_uid = request.getParameter("uid");
	String cur_pass = request.getParameter("cur_pass");
	String new_pass = request.getParameter("pass");
	String cur_email = request.getParameter("cur_email");
	String new_email = request.getParameter("email");
	String cur_fname = request.getParameter("cur_fname");
	String new_fname = request.getParameter("fname");
	String cur_lname = request.getParameter("cur_lname");
	String new_lname = request.getParameter("lname");
  
    ArrayList<String> fields = new ArrayList<String>();
    if(!new_uid.equals(cur_uid))
        fields.add("uid='" + new_uid + "'");
    if(!new_pass.equals(cur_pass))
        fields.add("pass='" + Hashing.HashSHA256(new_pass) + "'");
    if(!new_email.equals(cur_email))
        fields.add("email='" + new_email + "'");
    if(!new_fname.equals(cur_fname))
        fields.add("first_name='" + new_fname + "'");
    if(!new_lname.equals(cur_lname))
        fields.add("last_name='" + new_lname + "'");
    
    String update = "";
    for(String field : fields) {
        if(update != "")
        	update += ",";
        update += field;
    }
  
    Class.forName("com.mysql.jdbc.Driver");
	Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/foksite", "root", "root");
	Statement st = con.createStatement();
	st = con.createStatement();
	int i = st.executeUpdate("UPDATE users SET " + update + " WHERE (uid='" + cur_uid + "');");
	if (i > 0) {
		if(uid.equals(cur_uid) && !new_uid.equals(cur_uid))
	        session.setAttribute("uid", new_uid);
        
    	session.setAttribute("search_uid", request.getParameter("search_uid"));
        session.setAttribute("search_email", request.getParameter("search_email"));        
        session.setAttribute("search_fname", request.getParameter("search_fname"));
        session.setAttribute("search_lname", request.getParameter("search_lname"));
		%>
        <script type="text/javascript">
        //alert("Updated user info!");
        location.href = document.referrer;
        </script>
        <%
	} else {
		%>
        <script type="text/javascript">
        alert("Failed to update user info!");
        location.href = document.referrer;
        </script>
        <%
	}
%>