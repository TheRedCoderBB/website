<%@page import="fokc.website.SqlUtilities"%>
<%@ page import="java.sql.*"%>
<%
  String uid = session.getAttribute("uid").toString();
  if(SqlUtilities.getAccessLevel(uid) < 1) {
	  %>
      <script type="text/javascript">
      alert("Insufficient permission!");
      location.href = document.referrer;
      </script>
      <%
      return;
  }
  String commentid = request.getParameter("commentid");
  Class.forName("com.mysql.jdbc.Driver");
  Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/foksite", "root", "root");
  Statement st = con.createStatement();
  ResultSet rs;
  int hidden = 1;
  rs = st.executeQuery("SELECT hidden FROM comments WHERE (id=" + commentid + ");");
  if (rs.next()) {
      hidden = rs.getInt("hidden");
  }
  hidden = (hidden + 1) % 2;
  st = con.createStatement();
  int i = st.executeUpdate("UPDATE comments SET hidden=" + hidden + " WHERE (id=" + commentid + ");");
  if (i > 0) {
      //String action = hidden == 0 ? "shown" : "hidden";
      //alert("Post <% out.print(action); %>!");
	  %>
      <script type="text/javascript">
      location.href = document.referrer;
      </script>
      <%
	} else {
	  String action = hidden == 0 ? "show" : "hide";
      %>
      <script type="text/javascript">
      alert("Comment failed to <% out.print(action); %> post!");
      location.href = document.referrer;
      </script>
      <%
	}
%>