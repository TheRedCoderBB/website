<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>FokSite - Home</title>

<!-- Style References -->
<link rel="stylesheet" href="styles.css">

<style>
#header-index {
  font-weight: bolder;
}
</style>

</head>
<body>
  <jsp:include page="header.jsp"></jsp:include>
  <section id="main">
    <section class="main-block" style="width: 25%;">
      <p><span style="font-weight: bold;">About</span></p>
      <p>
      <span style="font-size: 16px;">
        Welcome to my website for the 5th point in computer science class!<br>
        <br>
        Visit <a class="main-link" href="updates.jsp">Updates</a> for updates and discussion<br>
        Visit <a class="main-link" href="youtube.jsp" style="color: red;">Youtube</a> for my recent videos<br>
      </span>
      </p>
    </section>
    <section class="main-block" style="width: 30%;">
      <p><span style="font-weight: bold;">Contact</span></p>
      <p>
        <form style="font-size: 16px;" autocomplete="off" id="contact" action="mailto:theredcoder2000@gmail.com">
          <input type="text"     class="login-text" placeholder="Full Name"     name="name"         required
               style="margin: 5px 0;"><span style="color: red;">&nbsp;*</span><br>
          <input type="email"    class="login-text" placeholder="Email Adress"  name="email"        required
                 style="margin: 5px 0;"><span style="color: red;">&nbsp;*</span><br>
          <span style="color: red; font: 16px Roboto Light;">* Required</span><span style="visibility: hidden;">&nbsp;*</span><br>
          <textarea class="post-comment-create-textarea" style="height: 100px;" name="contact-text" required></textarea><br>
          <section style="text-align: center; margin: 18px 0 0 0;">
            <button type="submit" class="login-button">Contact</button>
            <button type="reset" class="login-button">Reset</button>
          </section>
        </form>
      </p>
    </section>
  </section>
</body>
</html>