<%@page import="fokc.website.SqlUtilities"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>FokSite - Manage</title>

<!-- Style References -->
<link rel="stylesheet" href="styles.css">

<style>

</style>

</head>
<body>
  <%
  if(session.getAttribute("uid") == null) {
      response.sendRedirect("index.jsp");
      return;
  }
  String uid = session.getAttribute("uid").toString();
  %>
  <jsp:include page="header.jsp"></jsp:include>
  <section id="main">
    <section class="main-block" id="manage-me" style="width: 60%;">
      <%
      Class.forName("com.mysql.jdbc.Driver");
  	  Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/foksite", "root", "root");
      Statement st = con.createStatement();
  	  ResultSet rs;
  	  rs = st.executeQuery("SELECT * FROM users WHERE uid='" + uid + "';");
      rs.next();
      String user_uid = rs.getString("uid");
      String user_email = rs.getString("email");
      String user_fname = rs.getString("first_name");
      String user_lname = rs.getString("last_name");
      %>
      <div style="text-align: center;">
        Manage
      </div>
      <section style="margin: 10px;">
        <form autocomplete="off" method="post" action="functions/manage.jsp" id="manage-user">
          <input type="hidden"   name="cur_uid"     value="<%= user_uid %>">
          <input type="text"     name="uid"   placeholder="Username"   style="padding: 2px; width: 16%;"
                                                    value="<%= user_uid %>">

          <input type="hidden"   name="cur_pass"    value="">
          <input type="password" name="pass"        value=""           style="padding: 2px; width: 16%;" placeholder="&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;">
          
          <input type="hidden"   name="cur_email"   value="<%= user_email %>">
          <input type="text"     name="email" placeholder="Email"      style="padding: 2px; width: 16%;"
                                                    value="<%= user_email %>">
          
          <input type="hidden"   name="cur_fname"   value="<%= user_fname %>">
          <input type="text"     name="fname" placeholder="First name" style="padding: 2px; width: 16%;"
                                                    value="<%= user_fname %>">
          
          <input type="hidden"   name="cur_lname"   value="<%= user_lname %>">
          <input type="text"     name="lname" placeholder="Last name"  style="padding: 2px; width: 16%;"
                                                    value="<%= user_lname %>">
          
          <input type="submit"                      value="Apply"      style="padding: 5px; width: 10%; float: right;">
        </form>
      </section>
    </section>
    <% if(SqlUtilities.getAccessLevel(uid) > 0) { %>
    <section class="main-block" id="manage-others" style="width: 60%;">
      <div style="text-align: center;">
        Search
      </div>
      <%
      String search_uid = request.getParameter("search_uid");
      String search_email = request.getParameter("search_email");
      String search_fname = request.getParameter("search_uname");
      String search_lname = request.getParameter("search_lname");
      if(search_uid == null && session.getAttribute("search_uid") != null) search_uid = session.getAttribute("search_uid").toString();
      if(search_uid == null && session.getAttribute("search_email") != null) search_email = session.getAttribute("search_email").toString();
      if(search_uid == null && session.getAttribute("search_fname") != null) search_fname = session.getAttribute("search_fname").toString();
      if(search_uid == null && session.getAttribute("search_lname") != null) search_lname = session.getAttribute("search_lname").toString();
      %>
      <section style="margin: 10px;">
        <form autocomplete="off" method="post" action="manage.jsp" id="manage-search">
          <input type="text"     name="search_uid"   placeholder="Username"   style="padding: 2px; width: 16%;"
            <% if(search_uid != null) { %> value="<%= search_uid %>"<% } %>>
          
          <input type="password" name="search_pass"                           style="padding: 2px; width: 16%;"
            placeholder="&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;" disabled>
          
          <input type="text"     name="search_email" placeholder="Email"      style="padding: 2px; width: 16%;"
            <% if(search_email != null) { %> value="<%= search_email %>"<% } %>>
          
          <input type="text"     name="search_fname" placeholder="First name" style="padding: 2px; width: 16%;"
            <% if(search_fname != null) { %> value="<%= search_fname %>"<% } %>>
          
          <input type="text"     name="search_lname" placeholder="Last name"  style="padding: 2px; width: 16%;"
            <% if(search_lname != null) { %> value="<%= search_lname %>"<% } %>>
          
          <input type="submit"                             value="Apply"      style="padding: 5px; width: 10%; float: right;">
        </form>
      </section>
      <%
      if( !(
            (search_uid == null   || search_uid.isEmpty())   &&
	        (search_email == null || search_email.isEmpty()) &&
	        (search_fname == null || search_fname.isEmpty()) &&
	        (search_lname == null || search_lname.isEmpty())
           )
        ) {

    	%>
        <hr>
        <div style="text-align: center;">
          Results
        </div>
        <%
        
        ArrayList<String> conditions = new ArrayList<String>();
        if(search_uid != null && !search_uid.isEmpty())
          conditions.add("uid LIKE '%" + search_uid + "%'");
        if(search_email != null && !search_email.isEmpty())
            conditions.add("email LIKE '%" + search_email + "%'");
        if(search_fname != null && !search_fname.isEmpty())
            conditions.add("first_name LIKE '%" + search_fname + "%'");
        if(search_lname != null && !search_lname.isEmpty())
            conditions.add("last_name LIKE '%" + search_lname + "%'");
        String query_condition = "";
        for(String condition : conditions) {
          if(query_condition != "")
            query_condition += " AND ";
          query_condition += condition;
        }
    	st = con.createStatement();
    	rs = st.executeQuery("SELECT * FROM users WHERE (" + query_condition + ") LIMIT 10;");
        int i = 0;
    	while (rs.next()) {
        %>
          <%
          String cur_uid = rs.getString("uid");
          String cur_email = rs.getString("email");
          String cur_fname = rs.getString("first_name");
          String cur_lname = rs.getString("last_name");
          %>
          <section style="margin: 10px;">
            <form autocomplete="off" method="post" action="functions/manage.jsp" id="manage-user-<%= cur_uid %>">
              
              <input type="hidden"   name="cur_uid"     value="<%= cur_uid %>">
              <input type="text"     name="uid"   placeholder="Username"   style="padding: 2px; width: 16%;"
                                                        value="<%= cur_uid %>">
              
              <input type="hidden"   name="cur_pass"    value="">
              <input type="password" name="pass"        value=""           style="padding: 2px; width: 16%;" placeholder="&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;">
              
              <input type="hidden"   name="cur_email"   value="<%= cur_email %>">
              <input type="text"     name="email" placeholder="Email"      style="padding: 2px; width: 16%;"
                                                        value="<%= cur_email%>">
              
              <input type="hidden"   name="cur_fname"   value="<%= cur_fname %>">
              <input type="text"     name="fname" placeholder="First name" style="padding: 2px; width: 16%;"
                                                        value="<%= cur_fname %>">
              
              <input type="hidden"   name="cur_lname"   value="<%= cur_lname %>">
              <input type="text"     name="lname" placeholder="Last name"  style="padding: 2px; width: 16%;"
                                                        value="<%= cur_lname %>">
              
              <input type="hidden" name="search_uid"   value="<%= search_uid %>">
              <input type="hidden" name="search_email" value="<%= search_email %>">
              <input type="hidden" name="search_fname" value="<%= search_fname %>">
              <input type="hidden" name="search_lname" value="<%= search_lname %>">
              
              <input type="submit"                      value="Apply"      style="padding: 5px; width: 10%; float: right;">
            </form>
          </section>
          <%
          i++;
        }
        if(i == 0) {
          %>
            <span style="font-size: 14px; font-weight: bold; color: red;">No results found!</span>
          <%
        }
        %>
      <%
      }
      %>
    </section>
    <%
    }
    %>
  </section>
</body>
</html>