<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.*"%>
<%@page import="fokc.website.SqlUtilities"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>FokSite - Updates</title>

<!-- Style References -->
<link rel="stylesheet" href="styles.css">

<style>
#header-updates {
  font-weight: bolder;
}
</style>

</head>
<body>
  <jsp:include page="header.jsp"></jsp:include>
  <section id="main">
    <% if(session.getAttribute("uid") != null && SqlUtilities.getAccessLevel(session.getAttribute("uid").toString()) > 0) { %>
      <section class="post-post" id="post%post_id%">
        <section style="text-align: center;">
          <form id="post_create" autocomplete="off" method="post" action="functions/post.jsp">
            <textarea required class="post-comment-create-textarea" name="post_text"
                      style="width: 512px;
                             height: 128px;"></textarea>
            <br>
            <button type="submit" class="login-button">Post</button>
          </form>
        </section>
      </section>
    <% } %>
    <%
    Class.forName("com.mysql.jdbc.Driver");
	Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/foksite", "root", "root");
	Statement st = con.createStatement();
	ResultSet rs;
	rs = st.executeQuery("SELECT * FROM posts ORDER BY id DESC LIMIT 5;");
    int i = 0;
	while (rs.next()) {
        boolean hidden = rs.getInt("hidden") == 1;
        int access = SqlUtilities.getAccessLevel(session.getAttribute("uid"));
        if(hidden && access < 1)
          continue;
        i++;
        if(hidden) {
          %>
          <section style="opacity: 0.7">
          <%
        }
		%>
        <section class="post-post" id="post<%= rs.getInt("id") %>">
          <section class="post-creator">
            <section class="post-creator-image" id="user-<%= rs.getString("creator") %>-image" style="background-image: url('images/profiles/<%= rs.getString("creator").toLowerCase() %>.png');"></section>
            <div><%= rs.getString("creator") %>
            <% if(access >= 1) { %>
              <form id="post<%= rs.getInt("id") %>toggleform" method="post" action="functions/hide_post.jsp" style="float: right; padding: 0;">
                <span style="font-size: 14px; color: gray;">
                <input type="hidden" value="<%= rs.getInt("id") %>" name="postid">
                <input type="checkbox"
                       <% if(!hidden) { %> checked <% } %>
                       style="margin: 3px 0px -3px 20px;"
                       onclick="document.getElementById('post<%= rs.getInt("id") %>toggleform').submit();">
                Visible
                </span>
              </form>
            <% } %>
            <br>
            <div style="font-size: 12px;"><%
              SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy 'at' hh:mm:ss zzz");
              out.print(sdf.format(rs.getTimestamp("created")));
            %></div></div>
          </section>
          <section style="text-align: center;">
            <section class="post-main">
              <section class="post-text">
                <%= rs.getString("content") %>
              </section>
              <section class="post-comments">
                <%
                Statement st2 = con.createStatement();
            	ResultSet rs2;
            	rs2 = st2.executeQuery("SELECT * FROM comments WHERE (postid=" + rs.getInt("id") + ") ORDER BY id;");
                int j = 0;
                while(rs2.next()) {
                    boolean hidden2 = rs2.getInt("hidden") == 1;
                    if(hidden2 && access < 1)
                        continue;
                    if(hidden2) {
                        %>
                        <section style="opacity: 0.7">
                        <%
                    }
                    %>
                    <section class="post-comments-comment<%= (j++ % 2) + 1 %>">
                      <span style="color: black; font-weight: bold;"><%= rs2.getString("uid") %>:</span> <span><%= rs2.getString("content") %></span>
                      <% if(access >= 1) { %>
                          <form id="comment<%= rs2.getInt("id") %>toggleform" method="post" action="functions/hide_comment.jsp" style="float: right; padding: 0;">
                            <span style="font-size: 14px; color: gray;">
                            <input type="hidden" value="<%= rs2.getInt("id") %>" name="commentid">
                            <input type="checkbox"
                                   <% if(!hidden2) { %> checked <% } %>
                                   style="margin: 3px 0px -3px 20px;"
                                   onclick="document.getElementById('comment<%= rs2.getInt("id") %>toggleform').submit();">
                            Visible
                            </span>
                          </form>
                      <% } %>
                    </section>
                    <%
                    if(hidden2) {
                        %>
                        </section>
                        <%
                    }
                }
                if(j == 0) {
                    %>
                      <span>No comments! Be the first one!</span>
                    <% 
                }
                %>
              </section>
              <% if(session.getAttribute("uid") != null) { %>
                <form id="post<%= rs.getInt("id") %>comment" autocomplete="off" method="post" action="functions/comment.jsp">
                  <input type="hidden" value="<%= rs.getInt("id") %>" name="postid">
                  <textarea required class="post-comment-create-textarea" name="comment_text"></textarea>
                  <br>
                  <button type="submit" class="login-button">Comment</button>
                </form>
              <% } %>
            </section>
          </section>
        </section>
        <%
        if(hidden) {
            %>
            </section>
            <%
        }
	}
    if(i == 0) {
        %>
        <section class="post-post" id="no-posts">
          <section style="text-align: center;">
            <section class="post-text">
              <span style="color: red; font-weight: bold;">No posts yet! Stay tuned...</span>
            </section>
          </section>
        </section>
        <%
    }
    %>
  </section>
</body>
</html>