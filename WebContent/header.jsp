<%@page import="fokc.website.SqlUtilities"%>
<section id="header"
         style="position: absolute;
                text-align: left;
                top: 0;
                left: 0;
                width: 100%;
                margin: 0px;
                padding: 3px;
                font:30px Roboto Light;
                background-color: #f0f0f0;
                z-index: 10;">
  <a id="header-logo" class="darken"
           style="margin: -3px;
              padding: 3px 7px;
                  display: inline-block;
                  font:  inherit;
                  background-color: #e0e0e0;"
      href="index.jsp">
      FokSite
  </a>
  <section class="headerItem" style="width: 5px;"></section>
  <a id="header-index" class="shrink headerItem"
      href="index.jsp">
      Home
  </a>
  <a id="header-updates" class="shrink headerItem"
      href="updates.jsp">
      Updates
  </a>
  <section class="headerItem" style="width: 12px;"></section>
  <a id="header-youtube" class="shrink headerItem"
      href="youtube.jsp"
      style="color: red;">
      YouTube
  </a>
  <section id="header-login" class="sub-menu-parent" style="float: right; text-align: right; right: 50px;">
      <%
      if(session.getAttribute("uid") == null) {
    	  %>
          <a class="headerItem">Login</a>
          <form autocomplete="off" method="post" action="functions/login.jsp" id="header-login-menu" class="sub-menu">
            <input type="text"     class="login-text" placeholder="Username" name="uid"  required>
            <input type="password" class="login-text" placeholder="Password" name="pass" required>
            <section style="text-align: center">
              <button type="submit" class="login-button">Login</button>
              <button type="button" class="login-button" onclick="location.href='register.jsp'">Register</button>
            </section>
          </form>
          <% 
      } else {
          %>
          <a class="headerItem"><% out.print(session.getAttribute("uid")); %></a>
          <section id="header-login-menu" class="sub-menu">
            <section style="width: 150px;"></section>
            <span class="user-detail-text">Access level: <%
            int accessLevel = SqlUtilities.getAccessLevel((String)session.getAttribute("uid"));
            switch(accessLevel) {
            case 1:
                %> <span style="font-weight: bolder;">Admin</span> <%
                break;
            case 0:
                out.print("User");
                break;
            default:
                out.print("Error");
                break;
            }
            %></span>
            <section style="text-align: center">
              <button type="button" class="login-button" onclick="location.href='functions/logout.jsp'">Log Out</button>
              <button type="button" class="login-button" onclick="location.href='manage.jsp'">Manage</button>
            </section>
          </section>
          <%
      }
      %>
  </section>
</section>