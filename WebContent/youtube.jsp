<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>FokSite - YouTube</title>

<!-- Style References -->
<link rel="stylesheet" href="styles.css">

<style>
#header-youtube {
  font-weight: bolder;
}
</style>

</head>
<body>
  <jsp:include page="header.jsp"></jsp:include>
  <section id="main">
    <section class="main-block" style="size: auto; width: 820px; padding-top: 15px;">
      <iframe src="http://www.youtube.com/embed/?listType=user_uploads&list=TheRedCoder" width="800" height="600" allowfullscreen></iframe>
    </section>
  </section>
</body>
</html>